# FootBall Prediction

Features are as below

Name Description

HomeTeam Home Team

AwayTeam Away Team

FTR Full-Time Result (H=Home Win, D=Draw, A=Away Win)

HTHG Half Time Home Team Goals

HTAG Half Time Away Team Goals

HS Home Team Shots

AS Away Team Shots

HST Home Team Shots on Target

AST Away Team Shots on Target

AC Away Team Corners

HF Home Team Fouls Committed

AF Away Team Fouls Committed

HC Home Team Corners

HY Home Team Yellow Cards

AY Away Team Yellow Cards

HR Home Team Red Cards

AR Away Team Red Cards

Date On which day the match was played

league Under which league the match was played


$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$

Predict - FTR


No need to rerun the whole model - Only first time run is needed

The model uses train.csv to train and test to predict the FTR

The final file will be downloaded as result_svc.csv

$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$



Below were few training and test accuracy

Name	Train_score	Test_score			

KNN	KNN	0.710186	0.587351

LR	LR	0.659361	0.655291

NB	NB	0.641411	0.622417

DT	DT	0.684721	0.647777

RF	RF	0.717074	0.655604

GB	GB	0.671154	0.651221

Ada	Ada	0.661970	0.654352

XGB	XGB	0.675851	0.656544

ET	ET	0.748278	0.652160

SVM	SVM	0.673972	0.654039

SVML	0.663223	0.660927 == Seems to be best

MLP	MLP	0.656126	0.656230

$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$

Using SVML on test.csv to predict the FTR

$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$


In case of XGBoost Error 

Install XGBoost in Jupyter notebook as follows

Run the following command in notebook cell

import sys

!{sys.executable} -m pip install xgboost
